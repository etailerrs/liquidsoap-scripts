###
#
# Script will play the songs from the command line
#
###

#!/usr/bin/perl
use Net::Telnet ();

# Get the song from the CLI
my $filename = shift or die "Usage: $0 FILENAME\n";

# Telnet to default Liquidsoap port 1234
$t = new Net::Telnet (
		Timeout => 10,
		Port => '1234',
		Cmd_remove_mode =>      '1'
		);

$t->open("127.0.0.1");

# Print to log file
open($fh, ">>/tmp/play.log");
print $fh "Filename = ". $filename. "\n";
close($fh);

# Execute the command
$t->print("request.push $filename");

while($line = $t->getline(Errmode     => 'return', Timeout    =>      '2')) {
	print "$line<br>\n"; 
}

$t->close;
