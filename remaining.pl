###
#
# Script will get remaining play time in seconds for current song
#
###

#!/usr/bin/perl
use Net::Telnet ();

# Telnet to default Liquidsoap port 1234
$t = new Net::Telnet (
		Timeout => 10,
		Port => '1234',
		Cmd_remove_mode =>      '1'
		);

$t->open("127.0.0.1");

$t->print("music(dot)mp3.remaining");

$id = $t->getline(Errmode => 'return', Timeout => '2'); 
print int($id);

$t->close;
