###
#
# Script will print songs from the Liquidsoap order queue - Jukebox orders
#
###

#!/usr/bin/perl
use Net::Telnet ();

# Telnet to default Liquidsoap port 1234
$t = new Net::Telnet (
		Timeout => 10,
		Port => '1234',
		Cmd_remove_mode =>      '1'
		);

$t->open("127.0.0.1");

# Execute command
$t->print("request.queue");

# Get the result
@songs = split(/ /, $t->getline(Errmode => 'return', Timeout => '2'));
die "Order Queue is Empty!\n" if ($#songs == 0 && $songs[0] !~ m/^[0-9]/);

foreach $song (@songs) {
	if($song =~ m/^[0-9]/) {
		$t->print("request.metadata $song");
		while($line = $t->getline(Errmode => 'return', Timeout => '2')) {
			if($line =~ m/filename/) {
				print "$line\n";
			}
		}
	}
}

$t->close;
