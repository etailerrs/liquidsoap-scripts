Liquidsoap and icecast2 can be used to create Internet Radio station or stream your own music. You can 
configure Liquidsoap to have request queue and enable listeners to order songs like jukebox. These scripts, combined with
Apache2 CGI and Ajax can be used to create web based jukebox.

Liquidsoap Scripts
------------------

### music_next.pl

This script will print up to 10 next songs.

~~~
perl music_next.pl
~~~

### on_air.pl

Find what is the current song.

~~~
perl on_air.pl
~~~

### play_song.pl

Use it from command line to play MP3 file 

~~~
perl play_song.pl <mp3 file>
~~~

### queue.pl

Script will print songs from the request queue (if any)

~~~
perl queue.pl
~~~

### remaining.pl

Check remaining play time in seconds from the current song

~~~
perl remaining.pl
~~~