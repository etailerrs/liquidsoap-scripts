###
#
# Script will print up to 10 next songs from the Liquidsoap palylist
#
###

#!/usr/bin/perl
use Net::Telnet ();

# Telnet to default Liquidsoap port 1234
$t = new Net::Telnet (
		Timeout => 10,
		Port => '1234',
		Cmd_remove_mode =>      '1'
		);

$t->open("127.0.0.1");

# Execute the command - get id of the current song
$t->print("music.next");

# Print the result
$i = 0;
while(($line = $t->getline(Errmode     => 'return', Timeout    =>      '2')) && $i < 5) {
	print "$line\n"; 
	$i++;
}

$t->close;
