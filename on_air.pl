###
#
# Script will get the current song from the Liquidsoap queue.
# 
###

#!/usr/bin/perl
use Net::Telnet ();

# Telnet to default Liquidsoap port 1234 
$t = new Net::Telnet (
		Timeout => 10,
		Port => '1234',
		Cmd_remove_mode =>      '1'
		);
$t->open("127.0.0.1");

# Execute the command - get id of the current song
$t->print("request.on_air");

# Get the result
$id = $t->getline(Errmode => 'return', Timeout => '2'); 

# Execute the command - get metadata for id
$t->print("request.metadata $id");

# Get the result and print
while($line = $t->getline(Errmode => 'return', Timeout => '2')) {
	if($line =~ m/^filename/) {
		print "$line\n";

	}
}

$t->close;
